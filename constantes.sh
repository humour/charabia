#!/bin/bash

race=("elfe" "nain·e" "démon·e" "changelin·e" "orc·que" "cheval" "nuage" "amibe" "tiyanki" "ogre" "fée" "korrigan" "loup-garou" "revenant·e" "hobbit" "mort·e-vivant·e" "ermite" "humain·e" "troll" "poulet" "lapin" "liche" "banshee" "goule")
classe=("druide" "sorcier·e" "démoniste" "mage" "chaman" "guerrier·e" "voleur·se" "assassin·e" "troubadour" "nécromancien·ne" "chevalier" "ninja" "samouraï")
adjectif_debut=("puissant·e" "affreux·se"  "chaotique" "sinistre" "irréel" "merveilleux·se" "lunatique" "petit·e" "violent·e" "sanguinaire" "effrayant·e" )
adjectif_fin=("aveugle" "sourd·e" "muet·te" "sauvage" "assoiffé·e_de_sang" "en_plomb" "géant·e" "démoniaque" "unijambiste" "bleu·e" "rouge" "vert·e" "noir·e" "de_rue" "de_tigres" "de_licornes" "de_poulets" "de_dragons")
adjectif=(${adjectif_debut[*]} ${adjectif_fin[*]})
#echo ${adjectif[*]}
#echo "1="
#echo ${adjectif[1]}
metier=("cuisinier·e" "danseur·se" "mercenaire" "dresseur·se" "ivrogne" "prostitué·e" "tueur·se" "aubergiste" "tavernier·e" "montreur·se" "cordonnier·e" "tisserand·e" "pourfendeur·se" "éleveur·se")
nom_commun=("monde" "épée" "coffre" "chapeau" "âme" "sourire" "cœur" "espoir" "cul" "bâton magique" "grimoire" "armure" "château" "village")
proposition_but=("retrouver ton/ta meilleur·e ami·e" "sauver le monde" "détruire le monde" "tuer tes ennemi·e·s" "devenir un·e héros·ïne" "invoquer un·e démon·e" "te pendre" "élever des dragons" "retrouver ce que tu as perdu" "regagner ton honneur" "rejoindre le monde au-delà de l'arc-en-ciel")
adjectif_personnalite=("aimable" "puissante" "démoniaque" "sombre" "joyeuse" "effrayante" "lunatique" "sanguinaire" "joviale" "délirante" "chaotique" "séduisante" "mystérieuse")