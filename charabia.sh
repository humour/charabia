#!/bin/bash

source constantes.sh

echo "Tu es un·e ${race[$(( $RANDOM % 24  ))]} ${adjectif[$(( $RANDOM % 29 + 1 ))]}, élevé par un·e ${race[$(( $RANDOM % 24  ))]} ${classe[$(( $RANDOM % 13  ))]} qui t'a recueilli·e après la mort de tes parents, tués par des ${adjectif_debut[$(( $RANDOM % 11  ))]}s ${metier[$(( $RANDOM % 14  ))]}s ${adjectif[$(( $RANDOM % 29 + 1 ))]}s qui détruisirent ton ${nom_commun[$(( $RANDOM % 14  ))]}. À présent, tu es aux ordres d'un·e ${metier[$(( $RANDOM % 14  ))]} ${race[$(( $RANDOM % 24  ))]} et tu désires plus que tout ${proposition_but[$(( $RANDOM % 11  ))]}. Tu as une personnalité très ${adjectif_personnalite[$(( $RANDOM % 13  ))]}."
